export const environment = {
  firebase: {
    projectId: 'my-first-project-2ec91',
    appId: '1:155783647811:web:2adcfa71edb3f682763af6',
    databaseURL: 'https://my-first-project-2ec91-default-rtdb.firebaseio.com',
    storageBucket: 'my-first-project-2ec91.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyCc9vC_cj032VMR6H9DPfZ4fSGqE4f-nuc',
    authDomain: 'my-first-project-2ec91.firebaseapp.com',
    messagingSenderId: '155783647811',
    measurementId: 'G-3QYVDFFMPE',
  },
  production: true
};
