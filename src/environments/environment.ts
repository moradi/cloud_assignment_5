// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'my-first-project-2ec91',
    appId: '1:155783647811:web:2adcfa71edb3f682763af6',
    databaseURL: 'https://my-first-project-2ec91-default-rtdb.firebaseio.com',
    storageBucket: 'my-first-project-2ec91.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyCc9vC_cj032VMR6H9DPfZ4fSGqE4f-nuc',
    authDomain: 'my-first-project-2ec91.firebaseapp.com',
    messagingSenderId: '155783647811',
    measurementId: 'G-3QYVDFFMPE',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
