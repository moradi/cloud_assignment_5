import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/compat/auth";
import { GoogleAuthProvider, user } from "@angular/fire/auth";
import { AngularFirestoreDocument , AngularFirestore  } from "@angular/fire/compat/firestore";
import { Router } from '@angular/router';
import { Observable, of, switchMap } from 'rxjs';

import { User } from "./user";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // user$ : Observable<User> 
  // user = this.afs.doc<User>(`users/${this.user.uid}`).valueChanges() as Observable<User>;
  constructor(
    private router : Router,
    private fireauth: AngularFireAuth,
    private afs: AngularFirestore,
  ) {
    // this.user$ =  this.afs.doc<User>(`users/`).valueChanges() as Observable<User>
    fireauth.currentUser
   }

   googleSignIn(){
    this.fireauth.signInWithPopup(new GoogleAuthProvider).then(userRes => {
        console.log(userRes.user?.uid);
        this.router.navigate(['/dashboard'])
    }).catch(err=>console.log(err))

  }

  private updateUserData(user: User) {
    // Sets user data to firestore on login
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`);

    const data = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL
    };
  }
  currentUser(){
    return this.fireauth.currentUser
}

  async signOut() {
    await this.fireauth.signOut();
    this.router.navigate(['/']);
  }
}
