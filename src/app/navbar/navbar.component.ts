import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
// import { u } from 'firebase/aut';
import { User } from '../user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user?: User
  constructor(
    private auth: AuthService
  ) { 
    this.getCurrentUserName().then(cUser => {
       this.user = {
        uid : cUser?.uid as string,
        displayName: cUser?.displayName as string,
        photoURL: cUser?.photoURL as string,
        email: cUser?.email as string }
    }).catch(err=>console.log(err))

  }

  ngOnInit(): void {
  }
  logout(){
    this.auth.signOut()
  }

  getCurrentUserName() {
    return this.auth.currentUser()
  }
}
