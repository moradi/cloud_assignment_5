import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Movie } from './movie';
import { MOVIES } from './movies';


import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/compat/firestore';


@Injectable({
  providedIn: 'root'
})
export class MovieService {
  private moviesCollection: AngularFirestoreCollection<Movie>;
  movies: Observable<Movie[]>
  wishlist: Movie[] = [];
  constructor(
    private afs: AngularFirestore,
    // private afsc: AngularFirestoreCollection
  ) { 
    this.moviesCollection = afs.collection<Movie>('movies');
    this.movies = this.moviesCollection.valueChanges({idField: "id"})
  }
  getMovies(): Observable<Movie[]> {
    // const movies = of(MOVIES);
    return this.movies
  }
  filterMovies(searchText:string){
    // const allMovies = MOVIES
    // return MOVIES.filter(searchText)
  }
  getWishlist(): Observable<Movie[]>{
    const wishlist = of(this.wishlist)
    return wishlist
  }
  addToWishlist(addThis: Movie):void{
    
    this.wishlist.push(addThis)
    // console.log(this.wishlist);
    
  }
  
}
