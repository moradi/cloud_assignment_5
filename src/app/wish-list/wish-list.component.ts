import { Component, OnInit } from '@angular/core';
import { Movie } from '../movie';
import { MovieService } from '../movie.service';

@Component({
  selector: 'app-wish-list',
  templateUrl: './wish-list.component.html',
  styleUrls: ['./wish-list.component.css']
})
export class WishListComponent implements OnInit {
  wishlist : Movie[] = [];
  constructor(
    private movieService : MovieService
  ) { }

  ngOnInit(): void {
    this.getWishlist()
  }

  getWishlist():void{
    this.movieService.getWishlist().subscribe(wishlist => {
      this.wishlist = wishlist
      console.log(this.wishlist);}
      )
  }
}
