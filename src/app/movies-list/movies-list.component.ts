import { Component, Input, OnInit } from '@angular/core';
import { Movie } from '../movie';
import { MovieService } from '../movie.service';


@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.css']
})
export class MoviesListComponent implements OnInit {
 
  @Input() movies? : Movie[]
  @Input() isWishlist = true
  constructor( 
    private movieService: MovieService
  ) { }

  ngOnInit(): void {
   
  }

  addToWishlist(addThis: Movie): void {
    this.movieService.addToWishlist(addThis)
    this.movies?.map((movie) =>{
      if (movie.id === addThis.id){
       movie.inWishlist = true;
        
      }
    })
   
  }


}
