import { Movie } from "./movie";

export interface User {
    uid: string;
    email: string;
    photoURL?: string;
    displayName: string;
    whishlist?: Movie[];
}
